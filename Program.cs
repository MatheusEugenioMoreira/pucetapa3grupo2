﻿/*
Projeto: Fundamentos de Sistemas - Etapa 3
Grupo 2
Matheus Eugênio - 453592
Flávia Évanlen - 1516180
Matheus Junio – 1526870
Maria Eduarda – 1510644
Otávio Freitas – 1520053
Pedro Del Gruercio – 1521479
*/

using System;
using System.Collections.Generic;
using System.IO;

namespace EquipamentosValeCard
{
    class GerenciadorDeArquivoCSV
    {   
        public static void CriarArquivo(string caminhoDoArquivo, bool recria, string[] colunas)
        {
            
            if (!File.Exists(caminhoDoArquivo) || recria)
            {
                using (StreamWriter arquivo = File.CreateText(caminhoDoArquivo))
                {
                    arquivo.WriteLine(string.Join(",", colunas));
                }
            }
        }
        
        public static void InserirDados(string caminhoDoArquivo, params object[] dados)
        {
            using (StreamWriter arquivo = File.AppendText(caminhoDoArquivo))
            {
                arquivo.WriteLine(string.Join(",", dados));
            }
        }
        
        public static void ListarDados(string caminhoDoArquivo)
        {
            string? linha;
            List<string[]> linhas = new();
            
            using (StreamReader sr = File.OpenText(caminhoDoArquivo))
            {
                
                while ((linha = sr.ReadLine()) != null)
                {
                    linhas.Add(linha.Split(','));
                }
            }
            
            Console.WriteLine(new string('-', 50));
            Console.WriteLine(string.Format("| {0,-10} | {1,-20} | {2,-10} |", "Código", "Nome", "Preço"));
            Console.WriteLine(new string('-', 50));

            for (int i = 1; i < linhas.Count; i++) 
            {
                string[] valores = linhas[i];
                Console.WriteLine(string.Format("| {0,-10} | {1,-20} | {2,-10} |", valores[0], valores[1], valores[2]));
            }
            Console.WriteLine(new string('-', 50));
        }

        public static void ListarDadosSoftware(string caminhoDoArquivo)
        {
            string? linha;
            List<string[]> linhas = new();
            
            using (StreamReader sr = File.OpenText(caminhoDoArquivo))
            {
                
                while ((linha = sr.ReadLine()) != null)
                {
                    linhas.Add(linha.Split(','));
                }
            }

            Console.WriteLine(new string('-', 50));
            Console.WriteLine(string.Format("| {0,-10} | {1,-20} | {2,-15} |", "Código", "Nome", "Versão"));
            Console.WriteLine(new string('-', 50));
            
            for (int i = 1; i < linhas.Count; i++) 
            {
                string[] valores = linhas[i];
                Console.WriteLine(string.Format("| {0,-10} | {1,-20} | {2,-15} |", valores[0], valores[1], valores[2]));
            }
            Console.WriteLine(new string('-', 50));
        }

        public static Dictionary<int, string> ObterEquipamentos(string caminhoDoArquivo)
        {
            Dictionary<int, string> equipamentos = new();
            string? linha;
            using (StreamReader sr = File.OpenText(caminhoDoArquivo))
            {
                sr.ReadLine(); 
                while ((linha = sr.ReadLine()) != null)
                {
                    string[] valores = linha.Split(',');
                    equipamentos[int.Parse(valores[0])] = valores[1];
                }
            }
            return equipamentos;
        }

        public static Dictionary<int, string> ObterSoftwares(string caminhoDoArquivo)
        {
            Dictionary<int, string> softwares = new();
            string? linha;
            using (StreamReader sr = File.OpenText(caminhoDoArquivo))
            {
                sr.ReadLine(); 
                while ((linha = sr.ReadLine()) != null)
                {
                    string[] valores = linha.Split(',');
                    softwares[int.Parse(valores[0])] = valores[1];
                }
            }
            return softwares;
        }

        public static List<Tuple<int, int>> ObterAssociacoes(string caminhoDoArquivo)
        {
            List<Tuple<int, int>> associacoes = new();
            string? linha;
            using (StreamReader sr = File.OpenText(caminhoDoArquivo))
            {
                sr.ReadLine(); 
                while ((linha = sr.ReadLine()) != null)
                {
                    string[] valores = linha.Split(',');
                    associacoes.Add(new Tuple<int, int>(int.Parse(valores[0]), int.Parse(valores[1])));
                }
            }
            return associacoes;
        }

        public static void ListarEquipamentosESoftwares(string caminhoDoEquipamento, string caminhoDoSoftware, string caminhoDaAssociacao)
        {
            var equipamentos = ObterEquipamentos(caminhoDoEquipamento);
            var softwares = ObterSoftwares(caminhoDoSoftware);
            var associacoes = ObterAssociacoes(caminhoDaAssociacao);

            Console.WriteLine(new string('-', 60));
            Console.WriteLine($"| {"Equipamento",-20} | {"Software",-20} | {"Código",-10} |");
            Console.WriteLine(new string('-', 60));

            foreach (var equipamento in equipamentos)
            {
                bool temAssociacao = false;
                foreach (var associacao in associacoes)
                {
                    if (associacao.Item1 == equipamento.Key)
                    {
                        temAssociacao = true;
                        Console.WriteLine($"| {equipamento.Value,-20} | {softwares[associacao.Item2],-20} | {associacao.Item2,-10} |");
                    }
                }
                if (!temAssociacao)
                {
                    Console.WriteLine($"| {equipamento.Value,-20} | {"Nenhum software",-20} | {"-",-10} |");
                }
                Console.WriteLine(new string('-', 60));
            }
        }
    }

    class Programa
    {
        static void VincularEquipamentoASoftware(string caminhoDoArquivoEquipamento, string caminhoDoArquivoSoftware, string caminhoDoArquivoEquipamentoSoftware,
            int códigoDoEquipamento, int códigoDoSoftware)
        {
            string? linha;
            int código = -1;
            
            using (StreamReader sr = File.OpenText(caminhoDoArquivoEquipamento))
            {
                
                sr.ReadLine();
                while ((linha = sr.ReadLine()) != null)
                {
                    string[] valores = linha.Split(',');
                    código = int.Parse(valores[0]);
                    if (código == códigoDoEquipamento)
                        break;
                }
            }

            if (código == -1)
            {
                Console.WriteLine($"Código {código} de equipamento não existe!");
                return;
            }

            GerenciadorDeArquivoCSV.InserirDados(caminhoDoArquivoEquipamentoSoftware, códigoDoEquipamento, códigoDoSoftware);
        }

        static void IncluirEquipamento(string caminhoDoArquivoEquipamentos)
        {
            int códigoDoEquipamento;
            string? nomeDoEquipamento;
            double preçoDoEquipamento;

            Console.Write("Entre com o código do equipamento: ");
            códigoDoEquipamento = Convert.ToInt32(Console.ReadLine());

            Console.Write("Entre com o nome do equipamento: ");
            nomeDoEquipamento = Console.ReadLine();

            Console.Write("Entre com o preço do equipamento: ");
            preçoDoEquipamento = Convert.ToDouble(Console.ReadLine());

            GerenciadorDeArquivoCSV.InserirDados(caminhoDoArquivoEquipamentos, códigoDoEquipamento, nomeDoEquipamento, preçoDoEquipamento);
        }

        static void IncluirSoftware(string caminhoDoArquivoSoftware)
        {
            int códigoDoSoftware;
            string? nomeDoSoftware;
            string? versãoDoSoftware;

            Console.Write("Entre com o código do software: ");
            códigoDoSoftware = Convert.ToInt32(Console.ReadLine());

            Console.Write("Entre com o nome do software: ");
            nomeDoSoftware = Console.ReadLine();

            Console.Write("Entre com a versão do software: ");
            versãoDoSoftware = Console.ReadLine();

            GerenciadorDeArquivoCSV.InserirDados(caminhoDoArquivoSoftware, códigoDoSoftware, nomeDoSoftware, versãoDoSoftware);
        }

        static void ListarMenu()
        {
            Console.WriteLine("1 - Incluir Hardware");
            Console.WriteLine("2 - Listar Softwares");
            Console.WriteLine("3 - Incluir Software");
            Console.WriteLine("4 - Listar Software");
            Console.WriteLine("5 - Associar Hardware ao Software");
            Console.WriteLine("6 - Listar Hardware e seus Softwares");
            Console.WriteLine("0 - Sair do programa");
            Console.Write("Escolha uma opcao: ");
        }

        static void Main(string[] args)
        {
            string caminhoDoArquivoEquipamentos = "equipamentos.csv";
            string caminhoDoArquivoSoftware = "software.csv";
            string caminhoDoArquivoEquipamentoSoftware = "equipamento_software.csv";

            int opcao = -1;

            GerenciadorDeArquivoCSV.CriarArquivo(caminhoDoArquivoEquipamentos, false, new string[] { "Código", "Nome", "Preço" });
            GerenciadorDeArquivoCSV.CriarArquivo(caminhoDoArquivoSoftware, false, new string[] { "Código", "Nome", "Versão" });
            GerenciadorDeArquivoCSV.CriarArquivo(caminhoDoArquivoEquipamentoSoftware, false, new string[] { "CódigoDoEquipamento", "CódigoDoSoftware" });

            while (opcao != 0)
            {
                Console.Clear();
                Console.WriteLine("\nBem vindo ao programa de inventário de equipamentos da ValeCard!\n");
                ListarMenu();
                opcao = Convert.ToInt32(Console.ReadLine());
                Console.Clear();

                switch (opcao)
                {
                    case 1:
                        IncluirEquipamento(caminhoDoArquivoEquipamentos);
                        break;
                    case 2:
                        GerenciadorDeArquivoCSV.ListarDados(caminhoDoArquivoEquipamentos);
                        break;
                    case 3:
                        IncluirSoftware(caminhoDoArquivoSoftware);
                        break;
                    case 4:
                        GerenciadorDeArquivoCSV.ListarDadosSoftware(caminhoDoArquivoSoftware);
                        break;
                    case 5:
                        Console.Write("Entre com o código do equipamento: ");
                        int códigoDoEquipamento = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Entre com o código do software: ");
                        int códigoDoSoftware = Convert.ToInt32(Console.ReadLine());
                        VincularEquipamentoASoftware(caminhoDoArquivoEquipamentos, caminhoDoArquivoSoftware, caminhoDoArquivoEquipamentoSoftware, códigoDoEquipamento, códigoDoSoftware);
                        break;
                    case 6:
                        GerenciadorDeArquivoCSV.ListarEquipamentosESoftwares(caminhoDoArquivoEquipamentos, caminhoDoArquivoSoftware, caminhoDoArquivoEquipamentoSoftware);
                        break;
                    case 0:
                        Console.WriteLine("Saindo do programa...");
                        break;
                    default:
                        Console.WriteLine("Opção Inválida!");
                        break;
                }

                if (opcao != 0)
                {
                    Console.WriteLine("\nPressione qualquer tecla para voltar ao menu principal...");
                    Console.ReadKey();
                }
            }
        }
    }
}